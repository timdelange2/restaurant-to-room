var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstName: { type: String, required: 'Please check your first name'},
    lastName: { type: String, required: 'Please Check your last name'},
    roomNumber: { type: Number, required: 'Please check your room number', min:[100, 'Invalid room number'] },
    email: { type: String, required: 'Please Check your email'},
    password: { type: String, required: 'Please Check your password'},
    created: { type: Date, default: Date.now() }
});

var User = mongoose.model('User', userSchema);

module.exports = {
    User: User
};